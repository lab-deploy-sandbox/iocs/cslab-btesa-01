#########################################################
# File: RF_preamp.cmd                                   #
# BTESA Amplifier Module configuration communication    #
# UAC501R04: Amplifier module                           #
# Created in 03/2020 by Idoia Mazkiaran - ESS Bilbao    #
#########################################################

#require (modbus,2.9.0-ESS0)
require modbus,3.0.0
#require (RF_preamp,idoia)
#require (RF_preamp,local)
require btesa_preamp

# pulser Macros
epicsEnvSet("SECTION",    "MEBT")
epicsEnvSet("SUBSECTION", "010")
epicsEnvSet("DISCIPLINE", "RF")
epicsEnvSet("DEVICE",     "PreAmp")
epicsEnvSet("INDEX",      "001")

## MEBT-010:RF-preamp-001
epicsEnvSet("DEVICE",     "$(SECTION)-$(SUBSECTION):$(DISCIPLINE)-$(DEVICE)-$(INDEX)")

#epicsEnvSet("DEVICE1",       "$(DEVICE)")
epicsEnvSet("DEVICE1",       "RFLab:PreAmp")



epicsEnvSet("MODULENAME",       "RF_PreAmp")


#################### Commands for TCP/IP ####################
#
#
#############################################################
#drvAsynIPPortConfigure(char *portName,
#                       char *hostInfo,
#                       int priority,
#                       int noAutoConnect,
#                       int noProcessEos);
# priority,0, is the default priority.
# noAutoConnect,0, so asynManager will do
#                  an automatic connection management.
# noProcessEos,1,Modbus over TCP doesnt requiere
#                end-of-string processing.
#############################################################
#drvAsynIPPortConfigure("preampConnection","192.168.0.100:502",0,0,1)
#drvAsynIPPortConfigure("preampConnection","192.168.3.200:502",0,0,1)
#drvAsynIPPortConfigure("preampConnection","172.31.111.30:502",0,0,1)
#drvAsynIPPortConfigure("preampConnection","192.168.1.1:502",0,0,1)
drvAsynIPPortConfigure("preampConnection","172.30.6.22:502",0,0,1)



############################################################
#modbusInterposeConfig(const char *portName,
#                      modbusLinkType linkType,
#                      int timeoutMsec,
#                      int writeDelayMsec)
############################################################
modbusInterposeConfig("preampConnection",0,1000,0)
#modbusInterposeConfig("preampConnection",0,0,0)


#Give Asyn some time to successfully connect
sleep 1


############## MODBUS FUNCTION DEFINITIONS ###############
#
# portName -> Name of the modbus port to be created.
# tcpPortName -> Name of the asyn IP or serial port previously created.
# slaveAddress -> For TCP is used for the "unit identifier",
# bit before "function" bit in ADU.
# modbusFunction -> Function code.
# modbusLength ->The length of the Modbus data segment to be accessed.
# drvModbusAsynConfigure("portName",
#                        "tcpPortName",
#                         slaveAddress,
#                         modbusFunction,
#                         modbusStartAddress,
#                         modbusLength,
#                         dataType,
#                         pollMsec,
#                         "solidStatPwAmp")
##############################################################


########## FC2 -> Read Discrete Input ##########
#drvModbusAsynConfigure("FC2A", "preampConnection", 0,  2, 0x0200,  28,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2A", "preampConnection", 0,  2, 0x0200,  2,  0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC2B", "preampConnection", 0,  2, 0x0210, 12,  0, 500, "solidStatPwAmp")


########## FC3 -> Read Holding Register ##########
drvModbusAsynConfigure("FC3A", "preampConnection", 0,  3, 0x0001,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC3B", "preampConnection", 0,  3, 0x0002,   2, 0, 500, "solidStatPwAmp")


########## FC4 -> INPUT REGISTERS
drvModbusAsynConfigure("FC4A", "preampConnection", 0,  4, 0x0080,   1, 0, 500, "solidStatPwAmp")
drvModbusAsynConfigure("FC4B", "preampConnection", 0,  4, 0x0100,  16, 0, 500, "solidStatPwAmp")



########## FC6 -> Write Single Holding Register ##########
drvModbusAsynConfigure("FC6A", "preampConnection", 0,  6, 0x0001,   1, 0, 500, "solidStatPwAmp")

########## FC16 -> Write Multiple Holding Register ##########
drvModbusAsynConfigure("FC16", "preampConnection", 0, 16, 0x0002,   2, 0, 500, "solidStatPwAmp")




dbLoadRecords("$(btesa_preamp_DB)/RF_PreAmp.db", "DEVICE=$(DEVICE1)")
dbLoadRecords("$(btesa_preamp_DB)/RF_Out.db", "DEVICE=$(DEVICE1)")




